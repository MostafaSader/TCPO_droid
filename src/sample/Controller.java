package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    Button sendServer;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sendServer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               TCPClient tcp = new TCPClient(new TCPClient.OnMessageReceived() {
                   @Override
                   public void messageReceived(String message) {
                       System.out.println(message.toString());
                   }
               });
               tcp.start();
            }
        });
    }
}
