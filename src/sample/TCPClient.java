package sample;

//import com.jfoenix.validation.RequiredFieldValidator;
//import console.Commands;
//import controllers.MowerStatusController;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Sina on 3/29/2017
 */
public class TCPClient extends Thread {

    private String serverMessage;
    public String serverIP = "localhost"; //your computer IP address
    public int serverPort = 1234;
    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;

    private PrintWriter out;

    /**
     *  Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(){

    }
    public TCPClient(OnMessageReceived listener) {
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server
     * @param message text entered by client
     */
    public void sendMessage(String message ){
        if (out != null && !out.checkError()) {
            out.println(message);
            out.flush();
        }
//        if (!isFromCmd){
//            DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
//            Date dateobj = new Date();
//            System.out.println(df.format(dateobj) + " Odroid: |" + message);
//        }
    }
    public void setMessageListener(OnMessageReceived listener)
    {
        mMessageListener = listener;
    }

    public void stopClient(){
        mRun = false;
    }

    @Override
    public void run() {
//        super.run();

        mRun = true;

        try {
            //here you must put your computer's IP address.
            //InetAddress serverAddr = InetAddress.getByName(serverIP);
            System.out.println("C: Connecting...");
            //create a socket to make the connection with the server
            Socket socket = new Socket("localhost", serverPort);
            try {
                //send the message to the server
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                System.out.println("C: Sent.");

                System.out.println("C: Done.1");
//                MowerStatusController.connectBtn.setDisable(false);
//                MowerStatusController.connectingDialog.close();

                //receive the message which the server sends back
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

//                MowerStatusController.client.sendMessage("@HMIConnect\r\n" , false);
                //Commands.HMIConnect.sendThisCommand(false);
                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    serverMessage = in.readLine();

                    if (serverMessage != null && mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(serverMessage);
                    }
                    serverMessage = null;

                }

                System.out.println("S: Received Message: '" + serverMessage + "'");

            } catch (Exception e) {

//                System.out.println("S: Error1 + " + String.valueOf(e.toString()));
//                MowerStatusController.connectBtn.setDisable(false);
//                MowerStatusController.connectingDialog.setOverlayClose(false);
//                CommunicationBuffer.isConnected = false;
//                //TODO: check why it don't work
//                RequiredFieldValidator RFV = new RequiredFieldValidator();
//                MowerStatusController.portGrabber.getValidators().add(RFV);
//                RFV.setMessage("Not Connected!!");

            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
                System.out.println("C: Done.2");
//                MowerStatusController.connectBtn.setDisable(false);
//                CommunicationBuffer.isConnected = false;
//                MowerStatusController.connectingDialog.show();
            }

        } catch (Exception e) {
            System.out.println("C: Error2 " + String.valueOf(e.toString()));
//            MowerStatusController.connectingDialog.show();
//            MowerStatusController.connectBtn.setDisable(false);
//            MowerStatusController.connectingDialog.setOverlayClose(false);
//            CommunicationBuffer.isConnected = false;
//            MowerStatusController.portGrabber.validate();
        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented.
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }
}
