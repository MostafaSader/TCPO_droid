package sample;

//import controllers.MowerStatusController;

/**
 * Created by Sina on 4/1/2017
 */
public enum Commands {
    HMIConnect("@HMIConnect\r\n"),
    HMIStatsOn("@HMIStatsON\r\n"),
    HMIStatsOff("@HMIStatsOFF\r\n"),
    HMIStartAutoMowing("@HMIStartAutoMowing:"),
    HMIStopAutoMowing("@HMIStopAutoMowing\r\n"),
    HMIMovingSpeed("@HMIMovingSpeed:");

    private String textValue;

    Commands(String textValue) {
        this.textValue = textValue;
    }

    public String getTextValue() {
        return textValue;
    }

    public static boolean isOneOfUs(String isIt) {
        for (Commands value : values()) {
            if ((isIt + "\r\n").equals(value.textValue)) {
                return true;
            }
        }
        return false;
    }

//    public void sendThisCommand(boolean isFromCmd) {
//        MowerStatusController.client.sendMessage(textValue, isFromCmd);
//    }
//
//    public void sendStartAutoMowingCommand(String timer, boolean isFromCmd) {
//        MowerStatusController.client.sendMessage(textValue + timer + "\r\n", isFromCmd);
//    }
//
//    public void sendMovingSpeedCommnad(String value , boolean isFromCmd)
//    {
//        MowerStatusController.client.sendMessage(textValue + value + "\r\n" , isFromCmd);
//    }
}
