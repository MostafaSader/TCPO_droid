package sample;

import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Scanner;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        TCPServer server = new TCPServer(new TCPServer.OnMessageReceived() {
            @Override
            public void messageReceived(String message) {
                System.out.println(message);
            }
        });
        server.start();
        while (true) {
            server.sendMessage(new Scanner(System.in).nextLine()+"\r\n");
        }
    }
    public static void main(String[] args) {
        launch(args);
    }
}


